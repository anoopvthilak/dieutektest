import './App.css';
import { BrowserRouter, Route } from 'react-router-dom';
import Header from "./components/Header/Header.jsx";
import SignUp from "./components/SignUp/SignUp.jsx";
import Login from "./components/Login/Login.jsx";
import Dashboard from "./components/Dashboard/Dashboard.jsx";
import MyProvider from "./components/MyProvider.jsx";
import Loading from "./components/Loading/Loading.jsx";

function App() {
  return (
    <MyProvider> 
      <BrowserRouter>
          <Route exact path="/" component={SignUp} />
          <Route path="/login" component={Login} />
          <Route exact path="/dashboard" component={Dashboard} />
          <Route path="/dashboard" component={Header} />
          <Route path="/" component={Loading} />
      </BrowserRouter>
    </MyProvider> 
  );
}

export default App;
