import { Component } from "react";
import './Login.scss';
import DatePicker from 'react-date-picker';
import MyContext from '../MyContext.jsx';
import $ from 'jquery';
import {Link} from "react-router-dom";

class Login extends Component{
  constructor(props){
    super(props);
    this.state = {
      email : "",
      password : ""
    }

    this.form_on_change = this.form_on_change.bind(this);
    this.form_submit = this.form_submit.bind(this);
  }

  componentDidMount(){

  }

  form_on_change(e){
    this.setState({
      [e.target.name] : e.target.value
    });
  }

  form_submit(e){
    e.preventDefault();
    // console.log(this.state);
    // return;
    // const url = "http://localhost/Dieutek/dieutek_backend/public/api/signin";
    const url = "https://api.dieutek.boxmykart.com/api/signin";
    
    var obj_this = this;
    this.context.loading(1);
    $.ajax({
      url : url,
      data : {
        username : this.state.email,
        password : this.state.password
      },
      type : "POST"
    }).done(function(return_data){
      console.log(return_data);
      obj_this.context.loading(0);
      if(return_data.status == "success"){
        obj_this.context.setUserDetails(return_data);
        window.localStorage.setItem("user_details_dieutek_app",JSON.stringify(return_data));
        obj_this.props.history.push('/dashboard');
      }else{
        alert("Password or Email is wrong");
        obj_this.setState({'password' : ''})
      }

    });
  }
  render(){
    return(
      <MyContext.Consumer>
      {context => (
        <div className="login_page_container">
          <form className="login_form" onSubmit={this.form_submit}> 
            <div className="input_container">
              <div className="input_label">Email</div>
              <input name="email" 
                type="email"
                value={this.state.email} 
                onChange={this.form_on_change} 
                required
                autoComplete="off"
              />
            </div>
            <div className="input_container">
              <div className="input_label">Password</div>
              <input name="password" 
                type="password"
                value={this.state.password} 
                onChange={this.form_on_change} 
                required
                autoComplete="off"
              />
            </div>
            <div className="form_footer">
              <button className="login_button" type="submit">Login</button> <br/><br/>
              <Link className="create_new_button" to="/">Sign up</Link>
            </div>
          </form>
        </div>
       )}
    </MyContext.Consumer>
    )
  }
}

export default Login;
Login.contextType = MyContext;