import { Component } from "react";
import './SignUp.scss';
import DatePicker from 'react-date-picker';
import $ from 'jquery';
import MyContext from '../MyContext.jsx';


class SignUp extends Component{
  constructor(props){
    super(props);
    this.state = {
      fullname : "",
      mobile_no : "",
      email : "",
      dob : new Date(),
      password : ""
    }

    this.form_on_change = this.form_on_change.bind(this);
    this.DOB_on_change = this.DOB_on_change.bind(this);
    this.input_validation = this.input_validation.bind(this);
    this.form_submit = this.form_submit.bind(this);
  }

  componentDidMount(){
    $(".react-date-picker__inputGroup input").attr("readonly","true");
    $(".signup_button").on("click",function(){
      $(".input_error").css("display","none");
    });
    var obj_this =this;

    $(".ok_button").on("click",function(){
      obj_this.props.history.push('/login');
    })
  }

  form_on_change(e){
     $(".input_error").css("display","none");
    e.target.setCustomValidity("");    
    this.setState({
      [e.target.name] : e.target.value
    });
  }

  DOB_on_change(value){
    this.setState({"dob" : value})
  }

  input_validation(e){
    if(!e.target.validity.valid){
      var input_offset = $(e.target).offset();
      var input_width = $(e.target).width();
      $(".input_error").css({
        "left"  : input_offset.left,
        "top" : input_offset.top+35,
        "max-width" : input_width,
        "display" : "block"
      }).html($(e.target).attr("errMsg"));
    }
  }

  form_submit(e){
    e.preventDefault();
    // console.log(this.state);
    // return;
    // const url = "http://localhost/Dieutek/dieutek_backend/public/api/signUp";
    const url = "https://api.dieutek.boxmykart.com/api/signUp";
    this.context.loading(1);
    var obj_this = this;
    $.ajax({
      url : url,
      data : {
        user_data : JSON.stringify(this.state)
      },
      type : "POST"
    }).done(function(return_data){
      console.log(return_data);
      obj_this.context.loading(0);
      if(return_data.status == "success"){
        $(".ghost").css("display","block").find(".alert_message").html("Account created successfully, You may now login");
      }else if(return_data.status == "exists"){
        alert("Email already in use");
      }
    });
  }
  render(){
    return(
      <MyContext.Consumer>
      {context => (
        <div className="signup_page_container">
          <form className="signup_form" onSubmit={this.form_submit}> 
            <div className="form_title">Sign up</div>
            <div className="form_container">
              <div className="input_container">
                <div className="input_label">Fullname</div>
                <input name="fullname" 
                  type="text"
                  value={this.state.fullname}
                  onChange={this.form_on_change} 
                  required
                  autoComplete="off"
                />
              </div>
              <div className="input_container">
                <div className="input_label">Mobile number</div>
                <input name="mobile_no" 
                  type="tel"
                  value={this.state.mobile_no} 
                  onChange={this.form_on_change} 
                  onBlur={this.input_validation}
                  pattern="^[0]?[1-9][0-9]{9}$"
                  errMsg="Enter 10 digit mobile number"
                  onInvalid={(e)=>{e.target.setCustomValidity("Enter 10 digit mobile number")}}
                  required
                  autoComplete="off"
                />
              </div>
              <div className="input_container">
                <div className="input_label">Email</div>
                <input name="email" 
                  type="email"
                  value={this.state.email} 
                  errMsg = "Enter valid email"
                  onBlur={this.input_validation}
                  onInvalid={(e)=>{e.target.setCustomValidity("Enter valid email")}}
                  onChange={this.form_on_change} 
                  required
                  autoComplete="off"
                />
              </div>
              <div className="input_container">
                <div className="input_label">Date of birth</div>
                 <DatePicker
                  className="date_picker"
                  onChange={(value) => this.DOB_on_change(value)}
                  value={this.state.dob}
                  required
                  format="dd-MM-y"
                  maxDate = {new Date()}
                />
              </div>
              <div className="input_container">
                <div className="input_label">Password</div>
                <input name="password" 
                  type="password"
                  value={this.state.password} 
                  onChange={this.form_on_change} 
                  required
                  autoComplete="off"
                />
              </div>
            </div>
            <div className="form_footer">
              <button className="signup_button" type="submit">Sign up</button>
            </div>
          </form>
          <div className="input_error">
            Invalid
          </div>
          <div className="ghost">
            <div className="alert_box">
              <div className="alert_message"></div>
              <div className="ok_button">OK</div>
            </div>
          </div>
        </div>
      )}
    </MyContext.Consumer>
    )
  }
}

export default SignUp;
SignUp.contextType = MyContext;