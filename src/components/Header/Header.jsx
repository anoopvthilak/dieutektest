import {Component} from "react";
import "./Header.scss";
import $ from "jquery";
import MyContext from '../MyContext.jsx';

class Header extends Component {
  constructor(props){
    super(props);
  }
  componentDidMount(){
    var obj_this = this;
    $(".logout_button").on("click",function(){
      obj_this.context.setUserDetails("");
      window.localStorage.removeItem("user_details_dieutek_app");
      obj_this.props.history.push('/login');
    });
  }
  render(){
    return(
      <MyContext.Consumer>
      {context => (
        <div className="header_container">
          <div className="company_name">Dieutek</div>
          <div className="spacer"></div>
          <div className="user">Logout</div>
          <div className="logout_button">
            <i className="fas fa-sign-out-alt"></i>
          </div>
        </div>
        )}
      </MyContext.Consumer>
      )
  }
}

export default Header;
Header.contextType = MyContext;