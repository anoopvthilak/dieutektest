import {Component} from "react";
import './Dashboard.scss';
import MyContext from '../MyContext.jsx';


class Dashboard extends Component{
  constructor(props){
    super(props);
  }

  render(){
    return(
      <MyContext.Consumer>
      {context => (
        <div className="dashboard_page_container">
          <div className="page_title">Dashboard</div>
          <div className="welcome_message">
            Welcome, {this.context.user_details.fullname}
          </div>
        </div>
         )}
      </MyContext.Consumer>
      )
  }
}

export default Dashboard;
Dashboard.contextType = MyContext;